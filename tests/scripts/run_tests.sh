#!/usr/bin/bash
# make sure we have stress-ng installed
if rpm -q --quiet stress-ng; then
    :
else
    sudo dnf install -y stress-ng
    if [[ $? != 0 ]]; then
        echo "install of stress-ng failed!"
        exit 1
    fi
fi
 
# See if stress-ng is installed and executable
stress-ng --help 2>> /dev/null
if [[ $? != 0 ]]; then
    exit 2
fi
